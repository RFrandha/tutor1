from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
# Create your views here.
response = {}
def index(request):
	response['author'] = "Restow Frandha"
	html = 'lab_8/lab_8.html'
	return render(request, html, response)
